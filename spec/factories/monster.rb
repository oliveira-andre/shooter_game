# frozen_string_literal: true

FactoryBot.define do
  factory :monster do
    name { rand(2) }
  end
end
