# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    id { rand(100) }
  end
end
