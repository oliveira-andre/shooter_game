require 'rails_helper'

RSpec.describe KilledMonstersController, type: :controller do
  describe 'create killed monster' do
    let(:user) { FactoryBot.create(:user) }

    context 'creath a killed monster with success' do
      it 'create a killed monster and redirect to root page' do
        FactoryBot.create(:monster)
        post :create, params: { user_id: user.id }
        expect(KilledMonster.last.user_id).to eq(user.id)
        expect(response).to redirect_to(root_path(user_id: user.id))
      end
    end
  end
end
