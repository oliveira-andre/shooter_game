require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'index home' do
    context 'get death with success' do
      it 'create a user and get with success' do
        get :index
        expect(assigns(:user)).to eq(User.last)
        expect(response).to have_http_status(:successful)
      end
    end
  end
end
