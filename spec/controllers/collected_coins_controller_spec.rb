require 'rails_helper'

RSpec.describe CollectedCoinsController, type: :controller do
  describe 'create collected coins' do
    let(:user) { FactoryBot.create(:user) }

    context 'collect coin with success' do
      it 'create a collected coin and redirect to root page' do
        post :create, params: { user_id: user.id }
        expect(CollectedCoin.last.user_id).to eq(user.id)
        expect(response).to redirect_to(root_path(user_id: user.id))
      end
    end
  end
end
