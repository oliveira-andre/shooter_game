require 'rails_helper'

RSpec.describe TrophiesController, type: :controller do
  describe 'index trophies' do
    let(:user) { FactoryBot.create(:user) }

    context 'get trophies with success' do
      it 'get trophies information about user' do
        get :index, params: { user_id: user.id }
        expect(response).to have_http_status(:successful)
      end
    end
  end
end
