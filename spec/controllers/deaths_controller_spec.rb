require 'rails_helper'

RSpec.describe DeathsController, type: :controller do
  describe 'create death' do
    let(:user) { FactoryBot.create(:user) }

    context 'creath death with success' do
      it 'create a death and redirect to root page' do
        post :create, params: { user_id: user.id }
        expect(Death.last.user_id).to eq(user.id)
        expect(response).to redirect_to(root_path(user_id: user.id))
      end
    end
  end
end
