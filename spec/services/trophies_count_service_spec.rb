require 'rails_helper'

RSpec.describe TrophiesCountService do
  describe 'execute service' do
    context 'count trophies' do
      it 'show trophies of user equal to zero' do
        user = FactoryBot.create(:user)
        trophies = TrophiesCountService.execute(user.id)
        expect(trophies[:coins]).to eq(0)
        expect(trophies[:deaths]).to eq(0)
        expect(trophies[:turtles]).to eq(0)
        expect(trophies[:bowsers]).to eq(0)
      end

      it 'show trophies of user equal to one' do
        user = FactoryBot.create(:user)
        Monster.create(name: :turtle)
        Monster.create(name: :bowser)
        Death.create(user_id: user.id)
        CollectedCoin.create(user_id: user.id, value: 10)
        KilledMonster.create(user_id: user.id, monster: Monster.last)
        KilledMonster.create(user_id: user.id, monster: Monster.first)
        trophies = TrophiesCountService.execute(user.id)
        expect(trophies[:coins]).to eq(1)
        expect(trophies[:deaths]).to eq(1)
        expect(trophies[:turtles]).to eq(1)
        expect(trophies[:bowsers]).to eq(1)
      end
    end
  end
end
