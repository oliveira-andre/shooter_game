$(document).on('turbolinks:load', function () {
  $('.turtle').click(function() {
    $(this).val('1');
    $('#killed_monsters_form').submit();
  });

  $('.bowser').click(function() {
    $(this).val('1');
    $('#killed_monsters_form').submit();
  });

  $('.coin').click(function() {
    $('#collected_coins_form').submit();
  });

  setTimeout(function() {$('#deaths_form').submit()}, 2100);
});