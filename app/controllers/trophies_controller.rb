# frozen_string_literal: true

class TrophiesController < ApplicationController
  def index
    @user = params[:user_id]
    @trophies = TrophiesCountService.execute(@user)
  end
end
