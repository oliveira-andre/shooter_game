# frozen_string_literal: true

class DeathsController < ApplicationController
  def create
    Death.create(user_id: params[:user_id])
    redirect_to root_path(user_id: params[:user_id])
  end
end
