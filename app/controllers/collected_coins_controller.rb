# frozen_string_literal: true

class CollectedCoinsController < ApplicationController
  def create
    CollectedCoin.create(
      user_id: params[:user_id],
      value: 10
    )
    redirect_to root_path(user_id: params[:user_id])
  end
end
