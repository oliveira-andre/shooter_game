# frozen_string_literal: true

class KilledMonstersController < ApplicationController
  def create
    KilledMonster.create(
      user_id: params[:user_id],
      monster: Monster.last
    )
    redirect_to root_path(user_id: params[:user_id])
  end
end
