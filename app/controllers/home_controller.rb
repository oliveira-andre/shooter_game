# frozen_string_literal: true

class HomeController < ApplicationController
  before_action :create_user, only: :index
  before_action :create_monster, only: :index

  def index
    @coin = rand(2)
  end

  private

  def create_user
    @user = params[:user_id].present? ? User.find(params[:user_id]) : User.create
  end

  def create_monster
    if Monster&.last == KilledMonster&.last&.monster
      number = rand(2).zero?
      @monster = Monster.create(name: number ? :turtle : :bowser)
    else
      @monster = Monster.last
    end
  end
end
