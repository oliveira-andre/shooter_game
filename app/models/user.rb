# frozen_string_literal: true

class User < ApplicationRecord
  has_many :killed_monsters
  has_many :deaths
end
