# frozen_string_literal: true

class KilledMonster < ApplicationRecord
  belongs_to :monster
  belongs_to :user
end
