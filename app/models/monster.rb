class Monster < ApplicationRecord
  enum name: %i[turtle bowser]
  has_many :killed_monsters
end
