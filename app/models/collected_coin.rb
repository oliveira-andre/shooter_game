# frozen_string_literal: true

class CollectedCoin < ApplicationRecord
  belongs_to :user
end
