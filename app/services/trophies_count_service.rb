# frozen_string_literal: true

module TrophiesCountService
  class << self
    def execute(user_id)
      @user = user_id
      {
        coins: count_coins,
        deaths: count_deaths,
        turtles: count_turtles,
        bowsers: count_bowsers
      }
    end

    def count_coins
      collected_coins = CollectedCoin.where(user_id: @user).count
      count(collected_coins)
    end

    def count_deaths
      deaths = Death.where(user_id: @user).count
      count_deaths = 0 if deaths.zero?
      count_deaths = 1 if deaths.positive? && deaths < 9
      count_deaths = 1 if deaths > 9 && deaths < 24
      count_deaths = 1 if deaths > 24 && deaths < 49
      count_deaths = 1 if deaths > 49 && deaths < 99
      count_deaths = 1 if deaths >= 100
      count_deaths
    end

    def count_turtles
      turtles = Monster.turtle.ids
      count_deaths = KilledMonster.where(
        user_id: @user, monster_id: turtles
      ).count
      count(count_deaths)
    end

    def count_bowsers
      bowsers = Monster.bowser.ids
      count_deaths = KilledMonster.where(
        user_id: @user, monster_id: bowsers
      ).count
      count(count_deaths)
    end

    def count(number)
      result = 0 if number.zero?
      result = 1 if number.positive? && number < 99
      result = 2 if number > 99 && number < 999
      result = 3 if number > 999 && number < 9999
      result = 4 if number >= 100_000
      result
    end
  end
end
