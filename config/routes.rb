Rails.application.routes.draw do
  root 'home#index'

  resources :killed_monsters, only: :create
  resources :collected_coins, only: :create
  resources :deaths, only: :create
  resources :trophies, only: :index
end
